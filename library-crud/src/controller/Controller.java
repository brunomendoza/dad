package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.Window;

public class Controller implements ActionListener {
	private Window window;

	public Controller() {
		window = new Window(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		System.out.println("Button pressed");
	}

	public Window getWindow() {
		return window;
	}

	public void setWindow(Window window) {
		this.window = window;
	}
}
