package view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.Controller;

public class AuthorContentPane extends JPanel {
	private JTextField firstNameTextField;
	private JLabel firstNameLabel;
	private JLabel lastNameLabel;
	private JTextField lastNameTextField;
	private JLabel birthdateLabel;
	private JTextField birthdateTextField;
	
	private String submitString = "Insert Author";
	
	public AuthorContentPane(Controller controller) {
		GridBagLayout layout = new GridBagLayout();
		this.setLayout(layout);
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridx = 0;
		c.gridy = 0;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(10, 10, 0, 0);
		c.ipadx = 10;
		c.ipady = 10;
		
		firstNameLabel = new JLabel("First Name");
		this.add(firstNameLabel, c);
		
		c.gridx = 1;
		c.gridy = 0;
		c.insets = new Insets(10, 0, 0, 10);
		c.weightx = 0.5;
		
		firstNameTextField = new JTextField();
		this.add(firstNameTextField, c);
		
		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 0.0;
		c.insets = new Insets(10, 10, 0, 0);
		
	    lastNameLabel = new JLabel("Last Name");
	    this.add(lastNameLabel, c);
	    
	    c.gridx = 1;
	    c.gridy = 1;
	    c.insets = new Insets(10, 0, 0, 10);
	    
	    lastNameTextField = new JTextField();
	    this.add(lastNameTextField, c);
	    
	    c.gridx = 0;
	    c.gridy = 2;
	    c.insets = new Insets(10, 10, 0, 0);
	    
	    birthdateLabel = new JLabel("Birthdate");
	    this.add(birthdateLabel, c);
	    
	    c.gridx = 1;
	    c.gridy = 2;
	    c.insets = new Insets(10, 0, 0, 10);
	    
	    birthdateTextField = new JTextField();
	    this.add(birthdateTextField, c);
	    
	    c.gridx = 1;
	    c.gridy = 3;
	    c.weightx = 0.1; // Do nothing.
	    c.insets = new Insets(10, 0, 10, 10);
	    
	    JButton submitButton = new JButton(submitString);
	    submitButton.setActionCommand(submitString);
	    submitButton.addActionListener(controller);
	    this.add(submitButton, c);
	    
	    c.gridx = 0;
	    c.gridy = 4;
	    c.ipadx = 2;
	    c.ipady = 2;
	    c.gridheight = GridBagConstraints.REMAINDER;
	    c.insets = new Insets(10, 10, 10, 10);
	    
	    JLabel message = new JLabel("Some message");
	    message.setVisible(false);
	    this.add(message, c);
	}

	public JTextField getFirstNameTextField() {
		return firstNameTextField;
	}

	public void setFirstNameTextField(JTextField firstNameTextField) {
		this.firstNameTextField = firstNameTextField;
	}

	public JLabel getFirstNameLabel() {
		return firstNameLabel;
	}

	public void setFirstNameLabel(JLabel firstNameLabel) {
		this.firstNameLabel = firstNameLabel;
	}

	public JLabel getLastNameLabel() {
		return lastNameLabel;
	}

	public void setLastNameLabel(JLabel lastNameLabel) {
		this.lastNameLabel = lastNameLabel;
	}

	public JTextField getLastNameTextField() {
		return lastNameTextField;
	}

	public void setLastNameTextField(JTextField lastNameTextField) {
		this.lastNameTextField = lastNameTextField;
	}

	public JLabel getBirthdateLabel() {
		return birthdateLabel;
	}

	public void setBirthdateLabel(JLabel birthdateLabel) {
		this.birthdateLabel = birthdateLabel;
	}

	public JTextField getBirthdateTextField() {
		return birthdateTextField;
	}

	public void setBirthdateTextField(JTextField birthdateTextField) {
		this.birthdateTextField = birthdateTextField;
	}
}
