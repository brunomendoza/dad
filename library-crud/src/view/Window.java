package view;

import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import controller.Controller;

public class Window extends JFrame {
	Controller controller;
	
	public Window(Controller controller) {
		super("Lending Library");
		
		this.controller = controller;
		
	}
	
	public JMenuBar createMenuBar() {
		JMenuBar menuBar;
		JMenu menu, submenu;
		JMenuItem menuItem;
		
		// Create the menu bar.
		menuBar = new JMenuBar();
		
		// Build the first menu.
		menu = new JMenu("Insert");
		menu.setMnemonic(KeyEvent.VK_I);
		menu.getAccessibleContext().setAccessibleDescription("Insert");
		menuBar.add(menu);
		
		// A group of JMenuItems
		menuItem = new JMenuItem("Author", KeyEvent.VK_A);
		menuItem.getAccessibleContext().setAccessibleDescription("Insert author");
		menu.setActionCommand("");
		menu.add(menuItem);
		
		return menuBar;
	}
	
	
	
	public static void createAndShowGUI() {
		Controller controller = new Controller();
		controller.getWindow().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		controller.getWindow().setJMenuBar(controller.getWindow().createMenuBar());
		controller.getWindow().setContentPane(new AuthorContentPane(controller));
		
		controller.getWindow().pack();
		controller.getWindow().setVisible(true);
	}
	
	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
