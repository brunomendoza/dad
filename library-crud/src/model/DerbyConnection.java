package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DerbyConnection {
	private static final String protocol = "derby:jdbc:";
	private static final String dbName = "db/library";
	
	public static boolean getConnection() {
		Connection conn = null;
		
		try {
			conn = DriverManager.getConnection(protocol + dbName + ";create=true");
			System.out.println("Connected to and created database " + dbName);
			
			// We want to control transactions manually. Autocommit
			// is on by default.
			conn.setAutoCommit(false);
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		
		return true;
	}
	
	public void insertAuthor(int id, String firstName, String lastName, String birthdate) {
		
	}
	
	public void removeAuthor(int id, String firstname, String lastName, String birthdate) {
		
	}
	
	public void updateAuthor(int id) {
		
	}
	
	public void insertBook() {
		
	}
	
	public void removeBook() {
		
	}
	
	public void updateBook() {
		
	}
	
	public void insertWriting(int authorID, int bookID) {
		
	}
	
	public void removeWriting(int id) {
		
	}
	
	public void updateWriting(int id) {
		
	}
}
